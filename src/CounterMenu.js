import React from "react";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import "./CounterMenu.css";

class CounterMenu extends React.Component {
  handleSubmit = event => {
    event.preventDefault();

    const data = new FormData(event.target);
    const result = [];
    data.forEach((key, value) => result.push(key));
    //    if (Object.keys(result).length > 0) {
    this.props.onSuffixesSelected(result);
    //   }
  };

  render() {
    const checkboxes = this.props.items.map((key, index) => (
      <FormControlLabel
        key={index}
        control={<Checkbox value={key} name={key} />}
        label={key}
      />
    ));

    return (
      <form className="CounterMenu" onSubmit={this.handleSubmit}>
        <FormControl component="fieldset">
          <FormLabel component="legend">
            Choose counter suffixes to practise
          </FormLabel>
          <FormGroup className="CounterMenu-group">
            {checkboxes}
            <Button variant="contained" color="primary" type="submit">
              Start!
            </Button>
          </FormGroup>
        </FormControl>
      </form>
    );
  }
}

export default CounterMenu;
