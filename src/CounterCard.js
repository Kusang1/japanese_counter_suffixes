import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import * as wanakana from "wanakana";
import anime from "animejs";
import "./CounterCard.css";

class CounterCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: ""
    };
    this.card = React.createRef();
    this.answerRef = React.createRef();
  }

  onAnswerFocus = event => {
    wanakana.bind(event.target);
  };

  onAnswerBlur = event => {
    wanakana.unbind(event.target);
  };

  onAnswerChange = event => {
    this.setState({
      answer: event.target.value
    });
  };

  componentDidMount() {
    wanakana.bind(this.answerRef.current);
  }

  render() {
    return (
      <div ref={this.card}>
        <Card className="CounterCard">
          <CardMedia className="CounterCard-media" image={this.props.image} />
          <CardContent>
            <Typography
              className="CounterCard-number"
              gutterBottom
              variant="headline"
              component="h2"
            >
              x {this.props.amount}
            </Typography>
            <form onSubmit={this.handleSubmit.bind(this)}>
              <TextField
                id="answer"
                label="Answer"
                margin="normal"
                inputRef={this.answerRef}
                fullWidth
                autoFocus
              />
            </form>
          </CardContent>
        </Card>
      </div>
    );
  }

  handleSubmit(event) {
    event.preventDefault();
    const answer = wanakana.toHiragana(this.answerRef.current.value);
    if (answer.length > 0) {
      this.props.onAnswer(answer);
    }
  }

  createSuccessAnimation() {
    this.card.current.children[0].animate(
      [
        {
          transform: "rotateY(0deg) translateY(0px)",
          background: "#fff",
          opacity: 1
        },
        {
          transform: "rotateY(720deg) translateY(-200px)",
          background: "#00ff00",
          opacity: 0
        }
      ],
      {
        easing: "ease-in-out",
        fill: "forwards",
        duration: 1000
      }
    );
  }
}

export default CounterCard;
