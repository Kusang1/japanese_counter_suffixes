import React from "react";

class Display extends React.Component {
  render() {
    if (this.props["if"]) {
      return <div>{this.props.children}</div>;
    }
    return <div />;
  }
}

export default Display;
