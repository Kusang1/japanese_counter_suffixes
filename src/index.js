import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import FinishPage from "./FinishPage";
import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter, Route, Switch } from "react-router-dom";

ReactDOM.render(
  <BrowserRouter basename="/counters/">
    <div>
      <Switch>
        <Route exact={true} path="/" component={App} />
        <Route path="/finish" component={FinishPage} />
      </Switch>
    </div>
  </BrowserRouter>,
  document.getElementById("root")
);
registerServiceWorker();
