import React, { Component } from "react";
import { CSSTransitionGroup } from "react-transition-group";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import "./App.css";
import CounterCard from "./CounterCard";
import CounterMenu from "./CounterMenu";
import Display from "./Display";
import LinearProgress from "@material-ui/core/LinearProgress";
import { Redirect } from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      questions: {},
      total: 1,
      compleated: 0,
      cardsCounter: 0,
      finished: false
    };
  }

  async componentWillMount() {
    let response = await fetch("data.json");
    let data = await response.json();

    this.setState({
      questions: data
    });
  }

  onButtonClick = event => {
    let key = this.state.currentKey;
    let data = this.state.questions;
    let items = [data[key].numbers.pop()];
    this.setState({ items: items, questions: data });
  };

  handleSuffixes = data => {
    if (data !== null && data.length > 0) {
      let questions = this.state.questions;
      let newQuestions = {};
      let total = 0;
      data.forEach(x => {
        newQuestions[x] = questions[x];
        questions[x].numbers.sort(z => 0.5 - Math.random());
        total += questions[x].numbers.length;
      });

      this.setState({
        avaliableSuffixes: data,
        total: total
      });

      this.getRandomQuestion(newQuestions, data);
    }
  };

  onAnswer = userAnswer => {
    let answer = this.state.items[0];
    let audio;
    let questions = this.state.questions;
    let suffixes = this.state.avaliableSuffixes;
    let compleated = this.state.compleated;
    const key = this.state.currentKey;
    if (userAnswer === answer[1]) {
      audio = new Audio("sounds/correct.mp3");
      if (questions[key].numbers.length === 0) {
        const n = suffixes.indexOf(key);
        suffixes.splice(n, 1);
        delete questions[key];
      }
      compleated++;
      console.log("correct user answer");
    } else {
      audio = new Audio("sounds/incorrect.mp3");
      questions[key].numbers.unshift(this.state.items[0]);
      console.log(`incorrect user answer ${userAnswer}: ${answer[1]}`);
    }
    audio.play();
    let finished = suffixes.length === 0;
    let stateData = {
      avaliableSuffixes: suffixes,
      compleated: compleated,
      finished: finished,
      cardsCounter: this.state.cardsCounter + 1
    };

    if (finished) {
      stateData["items"] = [];
      this.setState(stateData);
    } else {
      this.setState(stateData);
      this.getRandomQuestion(questions, suffixes);
    }
    console.log(compleated, this.state.total);
  };

  getRandomImage() {
    const images = this.state.questions[this.state.currentKey].images;
    const n = Math.floor(images.length * Math.random());

    return images[n];
  }

  getRandomQuestion(questions, suffixes) {
    const keys = suffixes;
    const n = Math.floor(keys.length * Math.random());
    const key = keys[n];

    const items = [questions[key].numbers.pop()];

    this.setState({
      questions: questions,
      items: items,
      currentKey: key
    });
  }

  render() {
    let cards = this.state.items.map(x => (
      <CounterCard
        key={this.state.cardsCounter}
        image={this.getRandomImage()}
        amount={x[0]}
        answer={x[1]}
        onAnswer={this.onAnswer}
      />
    ));
    let suffixes = Object.keys(this.state.questions);
    return (
      <div className="App">
        <Display if={this.state.items.length === 0}>
          <CounterMenu
            items={suffixes}
            onSuffixesSelected={this.handleSuffixes}
          />
        </Display>
        <CSSTransitionGroup
          transitionName="example"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
        >
          {cards}
        </CSSTransitionGroup>
        <LinearProgress
          className="progress"
          variant="determinate"
          value={(this.state.compleated / this.state.total) * 100}
        />
        <Display if={this.state.finished}>
          <Redirect to="/finish" />
        </Display>
      </div>
    );
  }
}

export default App;
