import React from "react";
import { Link } from "react-router-dom";
import "./FinishPage.css";
class FinishPage extends React.Component {
  render() {
    return (
      <div className="FinishPage">
        <h1>That's it!</h1>
        <Link to="/">Again?</Link>
      </div>
    );
  }
}

export default FinishPage;
